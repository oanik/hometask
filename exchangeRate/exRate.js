/*Создайте приложение, в котором пользователь будет отправлять запросы по 
адресу: ‘https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json’ и получать 
текущий курс гривны по отношению к иностранным валютам. Отфильтруйте полученный список 
по уровню курса - только те элементы, у которых курс больше 25грн. 
В случае ошибки – отобразите ее пользователю в теле документа. 
В случае успеха – отобразите вывод данных в виде таблицы */

const get = (id) => {
	return document.getElementById(id);
};
//  Класс для  созданания объекта с таблицей и данными
class ExChangeRate {
	constructor(data) {
		this._data = data;
		this.cells;
		this.rows;
	}
	get data() {
		return this._data;
	}

	filterData() {
		const dataFiltered = this._data.filter((item) => item.rate >= 25);
		this._data = dataFiltered;
	}

	createTable() {
		this.cells = 0;
		this.rows = this._data.length;
		let counter = 0;
		for (let i = 0; i < this.rows; i++) {
			for (let key in this._data[i]) {
				counter++;
			}
			break;
		}
		this.cells = counter;

		const output = get("output");
		const table = document.createElement("table");

		for (let i = 0; i != this.rows; i++) {
			let row = document.createElement("tr");
			let objValue = Object.values(this._data[i]);
			let objKeys = Object.keys(this._data[i]);
			if (i == 0) {
				for (let j = 0; j < this.cells; j++) {
					let cell = document.createElement("td");
					let cellText = document.createTextNode(objKeys[j]);
					cell.classList.add("cells");
					cell.style.border = "1px solid black";
					cell.style.textAlign = "center";
					cell.append(cellText);
					row.append(cell);
				}
			} else {
				for (let j = 0; j < this.cells; j++) {
					let data = objValue[j];
					let cell = document.createElement("td");
					let cellText = document.createTextNode(data);
					cell.style.border = "1px solid black";
					cell.style.textAlign = "center";
					cell.append(cellText);
					row.append(cell);
				}
			}
			row.style.backgroundColor = "yellow";
			table.append(row);
		}
		output.append(table);
	}

	showData() {
		this.createTable();
	}
}
// отправка AJAX  запроса
function getDataAJAX() {
	const xhr = new XMLHttpRequest();
	xhr.open(
		"GET",
		"https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json"
	);

	const timeout = 2000;
	//  установка таймера на отмену запроса

	const timer = setTimeout(function () {
		xhr.abort();
		get("output").innerHTML = "Request is aborted";
	}, timeout);

	xhr.onreadystatechange = function () {
		if (xhr.readyState == 4 && xhr.status == 200) {
			clearTimeout(timer);
			[...data] = JSON.parse(xhr.responseText);
			const exchRate = new ExChangeRate(data);
			exchRate.filterData();
			exchRate.showData();
		}

		if (xhr.status === 404) {
			get("output").innerHTML = "Error";
		}
	};

	xhr.send();
}

window.onload = () => {
	get("button").addEventListener("click", getDataAJAX);
};
