const SIZE_SMALL = {
		name: "SIZE_SMALL",
		price: 50,
		calories: 20,
		size: "small",
		type: 2,
	},
	SIZE_LARGE = {
		name: "SIZE_LARGE",
		price: 100,
		calories: 40,
		size: "large",
		type: 2,
	},
	// Type - 0
	STUFFING_CHEESE = {
		name: "STUFFING_CHEESE",
		price: 10,
		calories: 20,
		type: 0,
		key: 1,
	},
	STUFFING_SALAD = {
		name: "STUFFING_SALAD",
		price: 20,
		calories: 5,
		type: 0,
		key: 2,
	},
	STUFFING_POTATO = {
		name: "STUFFING_POTATO",
		price: 15,
		calories: 10,
		type: 0,
		key: 3,
	},
	// type - 1
	TOPPING_MAYO = {
		name: "TOPPING_MAYO",
		price: 20,
		calories: 5,
		type: 1,
		key: 1,
	},
	TOPPING_SPICE = {
		name: "TOPPING_SPICE",
		price: 15,
		calories: 0,
		type: 1,
		key: 2,
	};

// обработка ошибки
function HamburgerException(message) {
	this.message = message;
	this.name = "Error";
}

class Hamburger {
	_topping = [];
	stuffing = [];
	constructor(size, stuffing) {
		this._size = this.setSize(size);
		this._stuffing = this.setStuffing(stuffing);
	}
	// проверка ввода размера и инит в конструкторе
	setSize(value) {
		if (value.type == 2) return (this._size = value);

		if (value == undefined) {
			throw new HamburgerException(`Hamburger::Invalid size ${value.name}`);
		}
		if (value.type != 2) {
			throw new HamburgerException(
				`Hamburger::Invalid input.Indicated not size, but ${value.name}`
			);
		}
	}
	// проверка ввода начинки и инит в конструкторе
	setStuffing(value) {
		if (value == undefined) {
			throw new HamburgerException(
				`Hamburger::Invalid stuffing type ${value.name}`
			);
		}
		if (value.type != 0) {
			throw new HamburgerException(
				`Hamburger::Invalid stuffing type ${value.name}`
			);
		}

		this.stuffing.push(value);
		return (this._stuffing = value);
	}
	// добавить топинг
	addTopping(value) {
		if (value.type == 1) {
			let result;
			if (this._topping.length != 0) {
				result = this._topping.find((item) => item.key == value.key);
				if (result) {
					throw new HamburgerException(
						`Hamburger::duplicate topping ${value.name}`
					);
				}
				this._topping.push(value);
				return;
			}
			this._topping.push(value);
			return;
		} else {
			throw new HamburgerException(`Hamburger::Invalid topping ${value.name}`);
		}
	}

	// добавить топинг
	removeTopping(value) {
		if (this._topping.length != 0) {
			let _index;
			for (let i = 0; i < this._topping.length; i++) {
				if (this._topping[i].key == value.key) {
					_index = i;
					this._topping.splice(_index, 1);
				}
			}
			return;
		}
		throw new HamburgerException(`Hamburger::No topping added ${value.name}`);
	}

	//добавить начинку
	addStuffing(value) {
		if (value.type == 0) {
			let result;
			if (this.stuffing.length != 0) {
				result = this.stuffing.find((item) => item.key == value.key);
				if (result) {
					throw new HamburgerException(
						`Hamburger::Duplicated stuffing ${value.name}`
					);
				}
				this.stuffing.push(value);
				return;
			}
			this.stuffing.push(value);
			return;
		} else {
			throw new HamburgerException(`Hamburger::Invalid stuffing ${value.name}`);
		}
	}

	// получить информацию о топинге
	getToppings() {
		this._topping.forEach((element) => {
			console.log(`Toppings are: ${element.name} \n`);
		});
	}

	// получить информацию о размере
	getSize() {
		return this._size.name;
	}

	// получить информацию о начинке
	getStuffing() {
		this.stuffing.forEach((element) => {
			console.log(`Stuffing:: ${element.name} \n`);
		});
	}

	//  посчитать цену
	calculatePrice() {
		let price = 0;
		price += this._size.price;

		this.stuffing.forEach((element) => {
			price += element.price;
		});

		this._topping.forEach((element) => {
			price += element.price;
		});

		console.log(`Price :: ${price} UAH`);
	}
	// посчитать каллории
	calculateCalories() {
		let calories = 0;
		calories += this._size.calories;

		this.stuffing.forEach((element) => {
			calories += element.calories;
		});

		this._topping.forEach((element) => {
			calories += element.calories;
		});

		console.log(`Calories:: ${calories} cal`);
	}
}

try {
	let hamburger = new Hamburger(SIZE_LARGE, STUFFING_CHEESE);
	hamburger.addTopping(TOPPING_MAYO);
	hamburger.addTopping(TOPPING_SPICE);
	hamburger.removeTopping(TOPPING_MAYO);
	hamburger.getToppings();
	hamburger.getSize();
	hamburger.addStuffing(STUFFING_SALAD);
	hamburger.getStuffing();
	hamburger.calculatePrice();
	hamburger.calculateCalories();
	hamburger.addStuffing(STUFFING_POTATO);
	hamburger.calculatePrice();
	hamburger.calculateCalories();
	console.log(
		"Is hamburger large: %s",
		hamburger.getSize() === SIZE_LARGE.name
	);
} catch (e) {
	if (e instanceof HamburgerException) {
		console.log(e);
	} else {
		throw e;
		console.log(e);
	}
}
