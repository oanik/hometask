function Tile() {
	this.opened = false;
	this.mine = false;
	this.numOfMinesAround = 0;
	this.row = 0;
	this.cell = 0;
}

const game_logic = {
	height: 8,
	width: 8,
	mine_number: 10,
	field: [],
	minesArray: [],
	fillTheField: function () {
		this.field = [];
		for (let i = 0; i < this.width; i++) {
			let arrOfrows = [];
			for (let j = 0; j < this.height; j++) {
				arrOfrows.push(new Tile());
			}
			this.field.push(arrOfrows);
		}

		for (let i = 0; i < this.mine_number; ) {
			let row = this.random(this.width);
			let col = this.random(this.height);
			if (!this.field[row][col].mine) {
				this.field[row][col].mine = true;
				i++;
			}
		}
	},

	random: function (value) {
		return parseInt(Math.random() * value - 0.0001);
	},

	countNumOfMinesAround: function (xCoord, yCoord) {
		let x_start = xCoord > 0 ? xCoord - 1 : xCoord;
		let y_start = yCoord > 0 ? yCoord - 1 : yCoord;
		let x_end = xCoord < this.width - 1 ? xCoord + 1 : xCoord;
		let y_end = yCoord < this.height - 1 ? yCoord + 1 : yCoord;
		let count = 0;
		for (let i = x_start; i <= x_end; i++) {
			for (let j = y_start; j <= y_end; j++) {
				if (this.field[i][j].mine && !(xCoord == i && yCoord == j)) {
					count++;
				}
			}
		}
		this.field[xCoord][yCoord].numOfMinesAround = count;
	},

	initMineCounter: function () {
		for (let i = 0; i < this.width; i++) {
			for (let j = 0; j < this.height; j++) {
				this.countNumOfMinesAround(i, j);
			}
		}
	},

	start: function () {
		this.fillTheField();
		this.initMineCounter();
	},
};

let game = {
	init: function () {
		this.game_interface.init();
		document.body.removeEventListener("click", this.init);
	},

	game_interface: {
		init: function () {
			game_logic.start();
			this.div = document.querySelector("#gameField");
			this.draw_ui();
			let self = this;
			this.div.addEventListener("click", function (e) {
				if (e.target.matches("td")) {
					self.is_open(e);
				}
			});
		},
		draw_ui: function () {
			this.div.innerHTML = "";
			const table = document.createElement("table");
			for (let i = 0; i < game_logic.width; i++) {
				const row = document.createElement("tr");
				for (let j = 0; j < game_logic.height; j++) {
					const cell = document.createElement("td");
					cell.classList.add("closedCell");
					cell.innerHTML = game_logic.field[i][j].numOfMinesAround;
					if (game_logic.field[i][j].mine) cell.style.backgroundColor = "red";
					row.append(cell);
				}
				table.appendChild(row);
			}
			this.div.appendChild(table);
		},

		showMinesOnField: function (elem, field) {
			let table = elem.firstElementChild;
			const [...rowArr] = table.childNodes;
			for (let row = 0; row < rowArr.length; row++) {
				let [...cellArr] = rowArr[row].children;
				for (let cell = 0; cell < cellArr.length; cell++) {
					if (game_logic.field[row][cell].mine) {
						let img = document.createElement("img");
						img.src = "./mine.jpeg";
						img.classList.add("cellWithMine");
						cellArr[cell].innerHTML = "";
						cellArr[cell].append(img);
					}
				}
			}
		},
		is_open: function (e) {
			y = e.target.cellIndex;
			x = e.target.parentNode.rowIndex;
			let openedCell = e;
			if (game_logic.field[x][y].mine) {
				this.showMinesOnField(this.div, game_logic.field);
			} else {
				console.log(game_logic.field);
				openedCell.target.classList.add("openedCell");
				openedCell.target.innerHTML = game_logic.field[x][y].numOfMinesAround;
			}
		},
	},
};

window.onload = () => {};
