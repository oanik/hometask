const get = (id) => {
	return document.getElementById(id);
};

class Slider {
	constructor() {
		this.imageArr = [];
		this.counter = 0;
		this.left = 0;
		this.right = 0;
		this.width = 300;
	}

	buttonPressed(event) {
		let target = event.target.value;
		switch (target) {
			case "prev":
				this.getPreviousPhoto();

				break;
			case "next":
				this.getNextPhoto();
				break;
			default:
				return;
		}
	}
	//  добавить первую ноду в концец коллекции IMG
	appendFirsNode() {
		let elemCollection = get("item");
		let firstNode = document.querySelector("img");
		let lastNode = document.querySelectorAll("img")[5];
		let clone = firstNode.cloneNode(true);
		elemCollection.append(clone);
		clone.style.left = parseInt(lastNode.style.left) + this.width + "px";
		firstNode.remove();

		const [...arrayImage] = document.querySelectorAll("#item img ");
		this.imageArr = arrayImage;
	}
	//  инициализировать координаты слайдов
	initImageArrCoord() {
		const [...arrayImage] = document.querySelectorAll("#item img ");
		this.imageArr = arrayImage;

		for (let i = 0; i < this.imageArr.length; i++) {
			if (i == 0) {
				this.imageArr[i].style.left = -600 + "px";
				this.imageArr[i].classList = "visible";
			} else {
				let xCoord = parseInt(this.imageArr[i - 1].style.left);
				this.imageArr[i].style.left = xCoord + this.width + "px";
				this.imageArr[i].classList = "visible";
			}
		}
	}

	//  показать спредыдущий слайд
	getPreviousPhoto() {
		this.appendFirsNode();

		for (let i = 0; i < this.imageArr.length; i++) {
			let xCoord = this.imageArr[i].style.left;
			xCoord = parseInt(xCoord) - this.width;
			this.imageArr[i].style.left = xCoord + "px";
		}
	}

	//  добавить последнюю ноду в начало коллекции IMG
	prepandLastNode() {
		const elemCollection = get("item");
		const lastNode = document.querySelectorAll("img")[5];
		const firstNode = document.querySelector("img");
		const clone = lastNode.cloneNode(true);
		elemCollection.prepend(clone);
		clone.style.left = parseInt(firstNode.style.left) - this.width + "px";
		lastNode.remove();

		const [...arrayImage] = document.querySelectorAll("#item img ");
		this.imageArr = arrayImage;
	}
	//  показать следующий слайд
	getNextPhoto() {
		this.prepandLastNode();

		for (let i = 0; i < this.imageArr.length; i++) {
			let xCoord = this.imageArr[i].style.left;
			xCoord = parseInt(xCoord) + this.width;
			this.imageArr[i].style.left = xCoord + "px";
			this.imageArr[i].style.display = "block";
		}
	}
}

const slider = new Slider();

window.addEventListener("DOMContentLoaded", () => {
	//отцентрировать родительский элемент Slider
	const element_slider = document.getElementById("slider");
	const element_slider_rect = element_slider.getBoundingClientRect();
	element_slider.style.position = "absolute";
	element_slider.style.left =
		element_slider_rect.left - element_slider_rect.width / 2 + "px";

	// проинициализировать позиции всех картинок
	slider.initImageArrCoord();

	//   повесить обработчик событий на кнопки prev и next
	const buttons = document.querySelector("#slider");
	buttons.addEventListener("click", function (e) {
		slider.buttonPressed(e);
	});
});
