//  класс Скидка

class Discount {
	constructor() {
		this.runningDiscout = document.querySelector("#banner");
	}
	//  добавить координаты элементу скидки
	assignCoordinates(event) {
		this.shiftX =
			event.clientX - this.runningDiscout.getBoundingClientRect().left + 10;
		this.shiftY =
			event.clientY - this.runningDiscout.getBoundingClientRect().top + 10;

		this.runningDiscout.style.position = "absolute";
		this.runningDiscout.style.zIndex = 1001;

		this.moveAt(event.pageX, event.pageY);
	}
	//  изменения координат элементу движением мыши
	moveAt(pageX, pageY) {
		this.runningDiscout.style.left = pageX - this.shiftX + "px";
		this.runningDiscout.style.top = pageY - this.shiftY + "px";
	}

	mouseMove(event) {
		this.moveAt(event.pageX, event.pageY);
	}
	// запуск события изменения координат элемента по движению мыши
	moveBanner() {
		this.runningDiscout.addEventListener(
			"mousemove",
			this.mouseMove(event.pageX, event.pageY)
		);
	}
}

// Форма по заполнению данных
class Form {
	constructor() {}
	getForm() {
		this.form = document.getElementsByName("info")[0];
		for (let i = 0; i < this.form.elements.length; i++) {
			console.log(this.form.elements[i].type);
		}
		let currentForm = null;
		let formValidation = false;

		for (let i = 0; i < this.form.elements.length; i++) {
			currentForm = this.form.elements[i];
			if (currentForm.type != "text") {
				continue;
			}

			const pattern = currentForm.getAttribute("data-val-msg-id");

			if (pattern) {
				currentForm.onchange = this.validateInput;
				formValidation = true;
			}
		}
		if (formValidation) {
			this.form.onsubmit = this.validateForm;
			this.form.onreset = this.resetForm;
		}
	}
	// валидация ввода
	validateInput() {
		let pattern, msg, msgId, value, check;
		switch (this.name) {
			case "name":
				pattern = /^([a-zа-яё]+)$/i;
				msg = this.dataset.valMsg;
				msgId = this.dataset.valMsgId;
				value = this.value;
				check = value.search(pattern);

				if (check == -1) {
					document.getElementById(msgId).innerHTML = "";
					document.getElementById(msgId).innerHTML = msg;
					document.getElementById(msgId).style.color = "red";
					this.style.backgroundColor = "red";
					this.style.color = "white";
				} else {
					document.getElementById(msgId).innerHTML = "";
					this.style.backgroundColor = "lightgreen";
					this.style.color = "black";
				}
				break;
			case "phone":
				pattern = /^((\+38)([0-9]){10})$/;
				msg = this.dataset.valMsg;
				msgId = this.dataset.valMsgId;
				value = this.value;
				check = value.search(pattern);

				if (check == -1) {
					document.getElementById(msgId).innerHTML = "";
					document.getElementById(msgId).innerHTML = msg;
					document.getElementById(msgId).style.color = "red";
					this.style.backgroundColor = "red";
					this.style.color = "white";
				} else {
					document.getElementById(msgId).innerHTML = "";
					this.style.backgroundColor = "lightgreen";
					this.style.color = "black";
				}
				break;
			case "email":
				pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
				msg = this.dataset.valMsg;
				msgId = this.dataset.valMsgId;
				value = this.value;
				check = value.search(pattern);

				if (check == -1) {
					document.getElementById(msgId).innerHTML = "";
					document.getElementById(msgId).innerHTML = msg;
					document.getElementById(msgId).style.color = "red";
					this.style.backgroundColor = "red";
					this.style.color = "white";
				} else {
					document.getElementById(msgId).innerHTML = "";
					this.style.backgroundColor = "lightgreen";
					this.style.color = "black";
				}
				break;
			default:
				console.log("Поля не існує");
		}
	}
	// валидация ввода
	validateForm() {
		let invalid = false;
		for (let i = 0; i < this.elements.length; i++) {
			let currentForm = this.elements[i];
			if (currentForm.type == "text" && currentForm.onchange != null) {
				currentForm.onchange();
				if (currentForm.style.backgroundColor == "red") invalid = true;
			}
		}
		if (invalid) {
			alert(" Допущені помилки при заповненні форми");
			return false;
		}
	}
	// сброс формы
	resetForm() {
		for (let i = 0; i < this.elements.length; i++) {
			let currentForm = this.elements[i];
			if (currentForm.type != "text") {
				continue;
			}

			let msgId = currentForm.dataset.valMsgId;
			let alertMsg = document.getElementById(msgId);
			alertMsg.innerHTML = "";
			currentForm.className = "";
			currentForm.style.dis == " ";
			currentForm.style.backgroundColor = "white";
		}
	}
}

// Пицца
function targetdragEnter(evt) {
	this.style.border = "3px solid red";
}
function targetdragLeave(evt) {
	this.style.border = "";
}
function targetdragOver(evt) {
	if (evt.preventDefault) evt.preventDefault();
	return false;
}
//  при перетаскивании ингредиентов  происходит проверка, был ли ранее добавлен соус. Давжды добавит соус нельзя
function tagetdrop(evt) {
	let id = evt.dataTransfer.getData("Text");

	if (id == "sauceBBQ" || id == "sauceClassic" || id == "sauceRikotta") {
		let sauce = arrayOfIngredients.find(
			(element) =>
				element.id == "sauceBBQ" ||
				element.id == "sauceClassic" ||
				element.id == "sauceRikotta"
		);
		if (sauce) {
			alert("Не можна додати  більше одного соусу");
		} else {
			if (evt.preventDefault) evt.preventDefault();
			if (evt.stopPropagation) evt.stopPropagation();

			this.style.border = "";

			let elem = document.getElementById(id);
			// добавляем элемент в целевой элемент. Так как в DOM не может быть два идентичных элемента - элемент удаляется со своей старой позиции.

			let leftPos = window.pageXOffset + this.getBoundingClientRect().left;
			let topPos = window.pageYOffset + this.getBoundingClientRect().top;
			console.log(leftPos, topPos);
			console.log(
				this.getBoundingClientRect().left,
				this.getBoundingClientRect().top
			);

			elem.style.left = leftPos + "px";
			elem.style.top = topPos + "px";
			elem.style.height = this.clientHeight + "px";
			elem.style.width = this.clientWidth + "px";
			elem.style.zIndex = 1000;
			elem.style.position = "absolute";

			document.body.append(elem);
			addIngrediateToTheList(elem);

			return false;
		}
	} else {
		if (evt.preventDefault) evt.preventDefault();
		if (evt.stopPropagation) evt.stopPropagation();

		this.style.border = "";

		//let id = evt.dataTransfer.getData("Text"); // получаем информации которая передавалась через операцию drag & drop

		let elem = document.getElementById(id);
		// добавляем элемент в целевой элемент. Так как в DOM не может быть два идентичных элемента - элемент удаляется со своей старой позиции.

		let leftPos = window.pageXOffset + this.getBoundingClientRect().left;
		let topPos = window.pageYOffset + this.getBoundingClientRect().top;

		elem.style.left = leftPos + "px";
		elem.style.top = topPos + "px";
		elem.style.height = this.clientHeight + "px";
		elem.style.width = this.clientWidth + "px";
		elem.style.zIndex = 1000;
		elem.style.position = "absolute";

		document.body.append(elem);
		addIngrediateToTheList(elem);

		return false;
	}
}

// добавить обработчкики событий на  целевой элемент drop
function addListenerforTarget(target) {
	target.addEventListener("dragenter", targetdragEnter, false);

	// перетаскиваемый элемент покидает область целевого элемента
	target.addEventListener("dragleave", targetdragLeave, false);

	target.addEventListener("dragover", targetdragOver, false);

	target.addEventListener("drop", tagetdrop, false);
}

/// Функции кнопок игредиетов пицци
//функция смены размера коржа и его цены
function changePizzaBase(e) {
	if (e.target.checked) {
		const pizzaBaseOption = e.target;
		const pizzaSize = Ingredients_Prices_Array.findIndex(
			(item) => item.name == pizzaBaseOption.id
		);
		if (pizzaSize == -1) {
			let obj = PRICES.find((item) => item.name == pizzaBaseOption.id);
			Ingredients_Prices_Array.shift();
			Ingredients_Prices_Array.unshift(obj);
			console.log(Ingredients_Prices_Array);
		}
	}
	showPrice(Ingredients_Prices_Array);
}
// функция добавления цены по дефолту большого коржа и обработчки событий на смену коржа
function choosePizzaSize() {
	const radioButton = document.forms.radioButton;
	const [...pizzaBaseOption] = radioButton.elements;
	pizzaBaseOption.forEach((element) => {
		if (element.checked) {
			let obj = PRICES.find((item) => item.name == element.id);
			Ingredients_Prices_Array.push(obj);
		}
	});

	radioButton.onchange = changePizzaBase;
}
//  Добавить игредиент в список и перенести игредиет и кнопку удаления в  поле  .result>sauces|| .result>topings
function addIngrediateToTheList(element) {
	const id = element.id;
	const span = document.getElementsByClassName(id)[0];
	const button = document.getElementsByClassName(id)[1];
	const sauceDiv = document.querySelector(".sauces");
	const toppingDiv = document.querySelector(".topings");
	let delButton = button.cloneNode(true);

	delButton.style.display = "inline-block";
	delButton.style.color = "white";
	delButton.style.backgroundColor = "blue";
	//delButton.borderRadius = "30%";
	const div = document.createElement("div");
	div.append(span, delButton);
	if (button.parentElement.className == "sauce") {
		sauceDiv.append(div);
	} else {
		toppingDiv.append(div);
	}
	button.remove();

	arrayOfIngredients.push(element);
	assignPrice(element, PRICES, Ingredients_Prices_Array);
	showPrice(Ingredients_Prices_Array);
}

function assignPrice(ingredient, prices, ingredientsList) {
	prices.forEach((item) => {
		if (item.name == ingredient.id) {
			let obj = {};
			obj.name = ingredient.id;
			obj.price = item.price;
			ingredientsList.push(obj);
		}
	});
}
////  Удалить игредиент из списока и перенести игредиет, картинку и кнопку удаления в первоначальное положени

// удалить кнопку, название ингедиента  из результата, вернуть ингредиент назад в список игредиентов
function deleteIngredButtonsFromTheResult(e) {
	const button = e.target;
	const itemID = button.className;
	if (button.value == "Delete") {
		const item = document.getElementById(itemID);
		const newItem = item.cloneNode(true);

		newItem.className = "ingridients";
		newItem.style.left = "";
		newItem.style.top = "";
		newItem.style.height = "";
		newItem.style.width = "";
		newItem.style.zIndex = 0;
		newItem.style.position = "";

		const id = item.id;
		const span = document.getElementsByClassName(id)[0];
		const button = document.getElementsByClassName(id)[1];
		const deleteButton = button.cloneNode(true);

		if (
			newItem.id == "sauceBBQ" ||
			newItem.id == "sauceClassic" ||
			newItem.id == "sauceRikotta"
		) {
			const parent = document.querySelectorAll(".sauce");
			parent.forEach((item) => {
				if (item.childElementCount == 0) {
					item.append(newItem, span, deleteButton);
				}
			});

			item.remove();
			button.remove();
		} else {
			const parent = document.querySelectorAll(".topping");
			parent.forEach((item) => {
				if (item.childElementCount == 0) {
					item.append(newItem, span, deleteButton);
				}
			});

			item.remove();
			button.remove();
		}
		deleteButton.style.display = "none";
		removePrice(newItem, Ingredients_Prices_Array);
		deletIngerdients(newItem, arrayOfIngredients);
		showPrice(Ingredients_Prices_Array);
		console.log(arrayOfIngredients);
		console.log(Ingredients_Prices_Array);
	}
}
// удалить  цену ингредиента из списка
function removePrice(ingredient, prices) {
	prices.forEach(function (item, index) {
		if (item.name == ingredient.id) {
			prices.splice(index, 1);
		}
	});
}
// удалить ингредиента из списка
function deletIngerdients(ingrediatent, ingredientsArray) {
	ingredientsArray.forEach((item, index) => {
		if (item.id == ingrediatent.id) {
			ingredientsArray.splice(index, 1);
		}
	});
}
//    показать цену за пиццу
function showPrice(prices) {
	let result = 0;
	let resultOutput = document.querySelector(".price>p");
	prices.forEach((item) => {
		console.log(item.price);
		result += item.price;
	});

	resultOutput.textContent = `Ціна:${result}`;
}
// функция добавления коржа в массив всех ингредиентов для drag&drop
function addPizzaBase() {
	const pizza = document.querySelector(".pizzaBase");
	arrayOfIngredients.push(pizza);
	addListenerforTarget(pizza);
}
///  захват и определения нужного ингредиента на нажатия мыши с последующим запуском функций события  drag
function captureIngredient(event) {
	if ((event.target.className = "img.draggable")) source = event.target;
	//   начало операции drag
	source.addEventListener(
		"dragstart",
		function (evt) {
			this.style.border = "3px dotted #000";
			evt.dataTransfer.effectAllowed = "move";
			evt.dataTransfer.setData("Text", this.id);
		},
		false
	);

	// конец операции drag
	source.addEventListener(
		"dragend",
		function (evt) {
			this.style.border = "";
		},
		false
	);

	addListenerforTarget(source);

	console.log(arrayOfIngredients);
	console.log(Ingredients_Prices_Array);

	//showPrice(Ingredients_Prices_Array);

	const result = document.querySelector(".result");
	result.addEventListener("click", deleteIngredButtonsFromTheResult);
}

//Глобальные переменные
const discount = new Discount();
const form = new Form();

const PRICES = [
	{
		name: "sauceClassic",
		price: 25,
	},
	{
		name: "sauceBBQ",
		price: 20,
	},
	{
		name: "sauceRikotta",
		price: 35,
	},
	{
		name: "moc1",
		price: 20,
	},

	{
		name: "moc2",
		price: 25,
	},
	{
		name: "moc3",
		price: 30,
	},
	{
		name: "telya",
		price: 30,
	},
	{
		name: "vetch1",
		price: 40,
	},
	{
		name: "vetch2",
		price: 42,
	},

	{
		class: "size",
		name: "small",
		price: 40,
	},
	{
		class: "size",
		name: "mid",
		price: 45,
	},
	{
		class: "size",
		name: "big",
		price: 50,
	},
];
const Ingredients_Prices_Array = [];
const arrayOfIngredients = [];

const dataset = document.querySelector(".ingridients");
let source;

// на загрузку страницы запускаем проверку формы,  выбор коржа и начинки + движение скидки
if (window.addEventListener)
	window.addEventListener(
		"load",
		() => {
			form.getForm();

			addPizzaBase();
			choosePizzaSize();
			dataset.addEventListener("mousedown", captureIngredient);

			discount.runningDiscout.onmouseover = discount.runningDiscout.onmouseout = function (
				event
			) {
				discount.assignCoordinates(event);
				discount.moveBanner();
			};
			discount.ondragstart = function () {
				return false;
			};
		},
		false
	);
