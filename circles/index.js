let isBodyClicked = false;
let initCircleDraw = () => {
	if (isBodyClicked === false) {
		const input = document.querySelector("input");
		let circleRadius = 0;
		if (input.value == " ") {
			return alert("No data");
		}
		circleRadius = Number(input.value);
		initCircleMatrix(circleRadius);
		document.body.removeEventListener("click", initCircleDraw);
		clickToRemoveCircle();
	}
	isBodyClicked = true;
};

let clickToRemoveCircle = () => {
	const [...arrayOfElemes] = document.querySelector("#mainDiv").children;
	arrayOfElemes.forEach((item) => {
		const [...elems] = item.children;
		elems.forEach((elementChild) => {
			elementChild.addEventListener("click", highlightThis, true);
		});
	});
};

const initCircleMatrix = (radius) => {
	const row = 10;
	const col = 10;
	const arrayOfColors = [
		"yellow",
		"pink",
		"blue",
		"red",
		"green",
		"brown",
		"white",
		"purple",
		"coral",
		"grey",
		"lightblue",
		"lightgreen",
		"khaki",
		"salmon",
	];
	const mainDiv = document.createElement("div");
	mainDiv.setAttribute("id", "mainDiv");
	for (let i = 0; i < row; i++) {
		const div = document.createElement("div");
		for (let j = 0; j < col; j++) {
			const indexOfColor = funcRandom(0, arrayOfColors.length - 1);
			let circle = new Circle(radius, arrayOfColors[indexOfColor]);
			div.append(circle);
		}
		mainDiv.append(div);
	}
	document.body.append(mainDiv);
};

const funcRandom = (min, max) => {
	return Math.floor(Math.random() * (max - min + 1));
};

function highlightThis() {
	alert(this.style.backgroundColor);
	this.remove();
}

let resetCircleDraw = () => {
	if (isBodyClicked === true) {
		const mainDiv = document.querySelector("#mainDiv");
		mainDiv.remove();
	}
	document.body.removeEventListener("click", resetCircleDraw);
	isBodyClicked = false;
};

function Circle(value, randomColor) {
	const div = document.createElement("div");
	div.style.width = value + "px";
	div.style.height = value + "px";
	div.style.border = "3px solid green";
	div.style.backgroundColor = randomColor;
	div.style.borderRadius = "50%";
	div.style.boxSizing = "border-box";
	div.style.display = "inline-block";
	return div;
}
